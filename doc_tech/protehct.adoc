= PROTEHCT 

== Innledning
Dette dokumentet er skrevet for å samle den tekniske dokumentasjonen som ligger til grunn for appliksajonen som er utviklet. Dokumentet er rettet mot flere parter med litt ulikt behov: 

* Forvaltere for å vite hva som installeres og for å vite hvordan applikasjonen kan konfigureres 
* EPJ forvaltere for å forstå hvilke informasjonsmodeller og kodeverk som er benyttet for å dokumentere data. 
* Utivklere som skal rette feil eller gjøre vedlikehold på løsningen. 

== Overview of the clnical traio.
Each patient will be evaluated 4 times based on a randomized protocol to select the radiologist. The following arms will be documented: 

* Control primary 
* Control secondary
* Experimental primary
* Experimental secondary

The primary investigation will be performed as routine follow up of patient. For each day the team will define who to take the control and experimental protocol. The selection will be done by flipping a coin. 
The secondary investigation will be performed by two pre-selected radiologists for the whole study. 

The following local terminology code is used in the Compositions to define which arm the data belongs to: 

[source]
----
PROTEHCT::CONTROL_PRIMARY::Primær kontroll arm
PROTEHCT::CONTROL_SECONDARY::Sekundær kontroll arm
PROTEHCT::EXPERIMENTAL_PRIMARY::Primær eksperimentell arm
PROTEHCT::EXPERIMENTAL_SECONDARY::Sekundær eksperimentell arm
----
