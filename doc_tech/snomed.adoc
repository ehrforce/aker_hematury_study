= SNOMED-CT koder 

== About 
The following is listing of the SNOMED-CT codes used in the trial. This document is used as a complete reference of the snomed codes used for better readability. The codes imported and *used* in the solution is listed in separate files under the folder link:terminologies[]. 

== Funn av kreft 

.link:terminologies/snomed_diagnoser.txt[] 
[source]
----
SNOMED-CT::372284002::No pathological findings
SNOMED-CT::399326009::Malignant tumor of urinary bladder (disorder)
SNOMED-CT::363458004::Malignant tumor of ureter (disorder) 
SNOMED-CT::363457009::Malignant tumor of renal pelvis (disorder)  
SNOMED-CT::702391001::Renal cell carcinoma (disorder) 
----

== Funn av steiner 

.link:terminologies/snomed_anatomi_steiner.txt[] 
[source]
----
SNOMED-CT::89837001::Urinary bladder structure (body structure)
SNOMED-CT::87953007::Ureteric structure (body structure) 
SNOMED-CT::64033007::Kidney structure (body structure) 
----


== Lateralitet for urinrør og nyre 
Dersom det er ønskelig for prosjetket kan det det innføres lateralitet for urinrør og nyre. Eksempel på dette er gitt under for nyre. 

[source]
----
SNOMED-CT::18639004::Left kidney structure (body structure) 
SNOMED-CT::9846003::Right kidney structure (body structure) 
----

== Prosedyrer 

.Referanse prosedyrer link:terminologies/snomed_referanse_prosedyrer.txt[]
[source]
----
SNOMED-CT::24139008::Cystoscopi  Endoscopy of urinary bladder (procedure) 
SNOMED-CT::361253001::Simple diagnostic ureteroscopy (procedure)(URS)
SNOMED-CT::609316007::Urincytologi,Microscopic cytologic examination of urine specimen (procedure) 
SNOMED-CT::86273004::Biopsy
----

== CT Prosedyrer 

.Overordnet undersøkelse link:terminologies/snomed_ct_us.txt[]
[source]
----
SNOMED-CT::419084009::CT of urinary tract
----

=== Detaljerte prosedyrer 
Ved behov kan det benyttes mer detaljerte koder for CT undersøkelsene. 
[source]
----
SNOMED-CT::440494008::CT of urinary tract with contrast
SNOMED-CT::764581000::CT nephrostography
SNOMED-CT::713859006::Excretory phase CT urography
----