// Dette er DIPS sin mal for brukerdokumentasjon (asciidoc).
// Målgruppen for dette dokumentet er sluttbrukere/superbrukere/forvaltere.
// Dokumenter basert på denne malen vil normalt ha "user_<programpakke (engelsk)>.adoc" som filnavn

:dips-template-url: .
include::{dips-template-url}\styles\dips-core-styles.adoc[]
include::{dips-template-url}\styles\dips-style-no.adoc[]


:product: PROTEHCT
:documenttype: Teknisk dokumentasjon


ifdef::backend-pdf[= {product}: {documenttype}]
ifdef::backend-html5[= {product} +++<BR>+++{documenttype}]
DIPS AS
:revnumber: Versjon 1.0
:revdate: 2019.10.21
:revremark: 

include::{dips-template-url}\styles\print.adoc[]

ifdef::print[]
include::{dips-template-url}\content\dips-copyright-notice.adoc[]

<<<

[[dips-document-history]]
[discrete]
#### Historikk
[options="header",frame="topbot",cols="15,10,20,55"]
|===
|Dato		|Revisjon	|Forfatter	|Beskrivelse
|2019-10-21 |1.0	|Bjørn Næss		|Ferdigstillelse av brukerdokumentasjon til første versjon
|===

endif::[]

<<<

ifeval::["{revremark}"=="Foreløpig"]
include::{dips-template-url}\content\dips-prerelease-notice-no.adoc[]
endif::[]


[[CONTENT]]

// tag for context sensitive hjelp
// tag::includetext[]

:product: PROTEHCT
:Config_Product_Oppsett_konfig-doc:

include::protehct.adoc[leveloffset=+1]

include::snomed.adoc[leveloffset=+1]

include::clinical_models.adoc[leveloffset=+1]

include::aql_binding.adoc[leveloffset=+1]

include::journal.adoc[leveloffset=+1]

include::paths.adoc[leveloffset=+1]


include::aql.adoc[leveloffset=+1]



include::vaqm.adoc[leveloffset=+1]

include::todo.adoc[leveloffset=+1]




//end::includetext[]

ifdef::backend-html5[]
<<<
include::{dips-template-url}\content\dips-copyright-notice.adoc[]
endif::[]
