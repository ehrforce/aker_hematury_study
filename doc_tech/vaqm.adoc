= VAQM Configuration

== Base 

[source]
----
SELECT c, o FROM  COMPOSITION c CONTAINS OBSERVATION o[openEHR-EHR-OBSERVATION.imaging_exam_result_dips.v1]
----


== AQL Binding

[source,xml]
----
  <AqlBinding Id="9bc3950e-d70d-43f4-9466-97528ddeb90c" Oid="study" Name="PROTEHCT Study" DefaultArchetypeId="openEHR-EHR-OBSERVATION.body_weight.v1">
      <BaseAql>SELECT c, o FROM  COMPOSITION c CONTAINS OBSERVATION o[openEHR-EHR-OBSERVATION.imaging_exam_result_dips.v1]</BaseAql>
      <Paths>
        <Path Id="ctxStartTime" IdentifierRef="c" Value="/context/start_time" DataType="DV_DATE_TIME" />
        <Path Id="TypeImageDiag" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value" AdminDescription="" DataType="DV_CODED_TEXT" />
        <Path Id="Method" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value" AdminDescription="" DataType="DV_CODED_TEXT" />
        <Path Id="AnatomyOfExamination" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.anatomical_location.v1]/items[at0001]/value" AdminDescription="" DataType="DV_CODED_TEXT" />
        <Path Id="Date" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[at0024]/value" AdminDescription="" DataType="DV_DATE_TIME" />
        <Path Id="NonCect" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.likert_classification_urinary_tract_cancer.v0]/items[at0001]/value" AdminDescription="" DataType="DV_ORDINAL" />mata[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.bosniak_classification.v0]/items[at0001]/value" AdminDescription="" DataType="DV_CODED_TEXT" />
        <Path Id="BosniakLaterality" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.bosniak_classification.v0]/items[at0007]/value" AdminDescription="" DataType="DV_CODED_TEXT" />
        <Path Id="HasStones" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.stones_in_urinary_tract.v0]/items[at0012]/value" AdminDescription="" DataType="DV_BOOLEAN" />
        <Path Id="HasStonsBladder" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.stones_in_urinary_tract.v0]/items[at0001]/items[at0002]/value" AdminDescription="" DataType="DV_BOOLEAN" />
        <Path Id="StoneSizeBladder" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.stones_in_urinary_tract.v0]/items[at0001]/items[at0003]/value" AdminDescription="" DataType="DV_QUANTITY" />
        <Path Id="HasStoneUreter" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.stones_in_urinary_tract.v0]/items[at0013]/items[at0014]/value" AdminDescription="" DataType="DV_BOOLEAN" />
        <Path Id="StoneSizeUreter" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.stones_in_urinary_tract.v0]/items[at0013]/items[at0015]/value" AdminDescription="" DataType="DV_QUANTITY" />
        <Path Id="HasStoneKidney" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.stones_in_urinary_tract.v0]/items[at0016]/items[at0017]/value" AdminDescription="" DataType="DV_BOOLEAN" />
        <Path Id="StoneSizeKidney" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.stones_in_urinary_tract.v0]/items[at0016]/items[at0018]/value" AdminDescription="" DataType="DV_QUANTITY" />
        <Path Id="DiagnosisLocation" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[at0020]/value" AdminDescription="" DataType="DV_CODED_TEXT" />
        <Path Id="Comment" IdentifierRef="o" Value="/data[at0001]/events[at0002]/data[at0003]/items[at0023]/value" AdminDescription="" DataType="DV_TEXT" />
        <Path Id="CTQuality" IdentifierRef="o" Value="/protocol[at0025]/items[openEHR-EHR-CLUSTER.image_quality.v0]/items[at0001]/value" AdminDescription="" DataType="DV_CODED_TEXT" />
      </Paths>
      <WhereExpressions>
        <WhereExpression Id="IsControl" Path="$Method/defining_code/code_string" Operator="EQ" Value="'CONTROL'" AdminDescription="" />
        <WhereExpression Id="IsExperimental" Path="$Method/defining_code/code_string" Operator="EQ" Value="'EXPERIMENTAL'" AdminDescription="" />
      </WhereExpressions>
      <OrderByExpressions>
        <OrderByExpression Id="LastComposition" Value="$ctxStartTime/value" Ascending="false" />
      </OrderByExpressions>
      <Predicates>
        <Predicate Id="LastControl" WhereRef="$IsControl" OrderByRef="$LastComposition" />
        <Predicate Id="LastExperimental" WhereRef="$IsExperimental" OrderByRef="$LastComposition" AdminDescription="" />
      </Predicates>
      <KeyValues />
    </AqlBinding>
----