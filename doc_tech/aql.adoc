= AQLs



== Experiment



|====
|Name|Path
|Image type
|/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value
|CT Date
|/protocol[at0025]/items[at0027]/items[at0034]/items[at0039 and name/value='CT Dato']/value
|Examination date
|/data[at0001]/origin
|Radiologist
|/protocol[at0025]/items[openEHR-EHR-CLUSTER.person_name.v0 and name/value='Radiolog']/items[at0001 and name/value='Radiolog']/value
|Method
|/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value
|Image quality
|/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.image_quality_protehct.v1]/items[at0001]/value
|TemplateName
|/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.template_semver_dips.v1]/items[at0001]/value
|TemplateMajor
|/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.template_semver_dips.v1]/items[at0003]/items[at0004]/value
|TemplateMinor
|/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.template_semver_dips.v1]/items[at0003]/items[at0005]/value
|TemplatePatch
|/context/other_context[at0001]/items[openEHR-EHR-CLUSTER.template_semver_dips.v1]/items[at0003]/items[at0006]/value
|====


[source,sql]
----
SELECT
    c/context/start_time/value AS CtxStarTime,
    o/protocol[at0025]/items[at0027]/items[at0034]/items[at0039]/value/value AS CtDato,
    o/protocol[at0025]/items[openEHR-EHR-CLUSTER.person_name.v0 and name/value='Radiolog']/items[at0001 and name/value='Radiolog']/value/value AS Radiolog,
    o/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.image_quality_protehct.v1]/items[at0001]/value/value AS ImageQuality
    --,o/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/defining_code/code_string AS TypeOfExamination,
    --. t/items[at0001]/value/value AS TemplateName,
    --.o/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value/defining_code/code_string AS Method
FROM
    COMPOSITION c
        ( CONTAINS CLUSTER t[openEHR-EHR-CLUSTER.template_semver_dips.v1] AND OBSERVATION o[openEHR-EHR-OBSERVATION.imaging_exam_result_dips.v1] )
WHERE
    t/items[at0001]/value/value = 'PROTEHCT'
    AND o/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value/defining_code/code_string = '419084009'
    AND o/data[at0001]/events[at0002]/data[at0003]/items[at0005]/value/defining_code/code_string = 'EXPERIMENTAL_SECONDARY'
LIMIT 10
----


[source, sql]
----
SELECT c,t,o FROM COMPOSITION c ( CONTAINS CLUSTER t[openEHR-EHR-CLUSTER.template_semver_dips.v1] AND OBSERVATION o[openEHR-EHR-OBSERVATION.imaging_exam_result_dips.v1] )
----


Dokumentconcept til VAQM 

$ARENA-HEALTHRECORD-DOCUMENTCONCEPT/PPR_klinisk_beslutning


$ARENA-HEALTHRECORD-DOCUMENTCONCEPT/PROTEHCT_CONTROL_PRIMARY
$ARENA-HEALTHRECORD-DOCUMENTCONCEPT/PROTEHCT_CONTROL_SECONDARY
$ARENA-HEALTHRECORD-DOCUMENTCONCEPT/PROTEHCT_EXPERIMENTAL_PRIMARY
$ARENA-HEALTHRECORD-DOCUMENTCONCEPT/PROTEHCT_EXPERIMENTAL_SECONDARY

== Studyadmin 

[source, sql]
----
select
   a/data[at0001]/items[at0002]/value as Study,
   a/data[at0001]/items[at0003]/value as Included,
   a/data[at0001]/items[at0005]/value as LastUpdated
FROM COMPOSITION c CONTAINS ADMIN_ENTRY a[openEHR-EHR-ADMIN_ENTRY.study_participation.v0]
 where a/data[at0001]/items[at0002]/value/value = 'PROTEHCT'
limit 10
----


$ARENA-HEALTHRECORD-DOCUMENTCONCEPT/PROTEHCT_PARTICIPATION


== Referense 

[source, sql]
----
select
   e/data[at0001]/items[at0002]/value as Cystoscopi, 
   e/data[at0001]/items[at0015]/value as Biopsi, 
   e/data[at0001]/items[at0007]/value as URS, 
   e/data[at0001]/items[at0028]/value as as Biopsi2,
   e/data[at0001]/items[at0011]/value as Urincytologi, 
   e/data[at0001]/items[openEHR-EHR-CLUSTER.paris_classification.v0]/items[at0001]/value as Paris, 
   e/protocol[at0019]/items[at0026]/value as LastUpdate
   
   
FROM COMPOSITION c CONTAINS  EVALUATION e[openEHR-EHR-EVALUATION.protehct_reference_standard.v1]

limit 10
----

$ARENA-HEALTHRECORD-DOCUMENTCONCEPT/PROTEHCT_REFERENCE