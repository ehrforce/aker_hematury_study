= Journalstruktur 

== Status

[horizontal]
08.oktober 2019:: Oppdatert til versjon 1.3.0 av skjema(form)
26.september 2019:: Initiell utkast 

== Innledning 
I det følgende beskrives dokumenttyper og -maler for PROTEHCT studien. Det skal utvikles en DUP for å kunne importere dette inn i et kjørende 16.x miljø på OUS. 

WARNING: Det er ikke lagt inn nummer for dokumenttype- og mal ID. Utvikler som gjør jobben må legge inn dette i henhold til retningslinje for oppsett av DIPS dokumenttyper - og maler. 

.Dokumenttyper, skjema og dokumentmaler
[options="header",frame="topbot"]
|====
|Navn | Type | Mal |  Form  | Versjon | Beskrivelse
6+^h|Dokumenter for administrasjon og oppfølging av studien 
| PROTEHCT Studieadministrasjon | -2205 | utvagk+0000001002218 
| PROTEHCT_Administration_Participation |1.3.0
| Dokument som brukes for administrasjon av PROTEHCT studien  

 |PROTEHCT Referansestandard | -2207 | utvagk+0000001002220
|PROTEHCT_Referanse_standard |1.3.0
| Dokument for løpende oppdatering av referansestandard

6+^h| Dokumenter/skjema for registrering av data til studien 
| PROTEHCT Kontroll Primær  | -2209 | utvagk+0000001002222 
|PROTEHCT_Examination_Control_Primary |1.3.0
| Ordinær kontrollundersøkelse av pasient. 

| PROTEHCT Kontroll Sekundær | -2211 | utvagk+0000001002224 
|PROTEHCT_Examination_Control_Secondary |1.3.0
| Kontrollundersøkelse som sekundær granskning

| PROTEHCT Eksperimentell Primær | -2213 | utvagk+0000001002226
| PROTEHCT_Examination_Experimental_Primary |1.3.0
| Ekseprimentell granskning, primær

| PROTEHCT Eksperimentell Sekundær | -2215 | utvagk+0000001002228 
|PROTEHCT_Examination_Experimental_Secondary|1.3.0
| Eksperimentell granskning, sekundær 

|====

Beskrivelse av kolonner

[horizontal]
Navn:: Betegnelsen på dokumentet
Type:: Dokumenttype ID 
Mal:: Dokumentmal ID 
Form:: Skjema ID 
Versjon:: Versjon av skjema som benyttes 
Beskrivelse :: Beskrivelse av dokumentet 
