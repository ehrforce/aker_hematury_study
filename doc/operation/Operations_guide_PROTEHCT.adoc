////
DIPS template for a software package in Operations Guide.
Module in this context correspond to a Module in Octopus-template repo.
The template cannot be used as the main document, as it does not contain the cover page, table of contents, history and approval table.
The indended audience for this document is system architects and operators.
See http://utvikling/t/retningslinjer-for-produksjon-av-dips-arena-operations-guide/1309 
////

:dips-template-url2: .\confprod

:module: PROTEHCT
:revnumber: 1.0.0
:chocopackage: Chocopackage name.{revnumber}

== {module}

PROTEHCT is a clinical trial at OUS, Aker. This application is developed to support the clinicians collecting data for the research. 

=== Installation

PROTEHCT is developed as a configurable product. All the steps could have been performed by manual installations and configuration. 

A DUP was made to simplify the steps related to clinical models and document types7templates. In addition to the DUP there are some manual steps to be performed. 

=== DUP Installation


include::_dup.adoc[]



=== Configuration

. Install the DUP 
. Connect the documenttypes to the wanted `Journalgruppe`
. Configure the document template ids for the AQL binding 
. Update AQL binding configuration
. Configure the patientlist 
. Set up the patientlist with the correct dataelement (access control) 
. Test and verify 

==== Connect to Journalgruppe 
The `Journalgruppe` configuration is system-specific. It is not provided by the DUP. As an administrator you must connect the documenttype to the right `Journalgruppe`. 

==== Dokumenttype for AQL binding
New `Dokumentmal` ids is generated in the system when the DUP is run. As an administrator go to `Dokumentadmin` and find the `Dokumentmal` id for each document template. These ids MUST be configured into the AQL binding. 

NOTE: The vendor might change the AQL binding configuration based on feedback from OUS, or we can set this up onsite. 

==== Update AQL configuration 
After the step above you must update the AQL configuration in the system. As you know; this step will be much easier after upgrading and the usage of VAQM. But for now you must manually add the Aqlbinding and Dataelement to the configuration. 

WARNING: Please take a copy of the existing AQL configuration before starting. 



==== Patientlist configuration 
This is the easy step. 

. Create a new patientlist
. Select rowprovided `Folder` and folder type `Cancer trajectory`
. Add the wanted columns with at least the ones for the PROTEHCT study. See user doc for information. 


=== Verification
Verify the product by checking that there is documenttemplates according to the user-documentation. 

=== Monitoring and Operations
////
Describe how to monitor and operate the module 
////

=== Troubleshooting
////
Describe troubleshooting procdure for the module 
////
