@echo off

set docname=Operations_guide_PROTEHCT
set outdoc=DIPS.Protehct.Operations
set adoc=%docname%.adoc
set stylecss=dips.css

if not defined docname (set docname=index)
if not defined outdoc (set outdoc=%docname%)

echo Genererer xhtml5 fra asciidoc filen %adoc%
call  asciidoctor -r asciidoctor-diagram  -D . -a stylesheet=%stylecss% -a linkcss -b xhtml5 %adoc% -o %outdoc%.html

