////
Der hvor det står ".N+", bytt ut N med antall rader som skal være i andre kolonne. Dette er for å kunne liste opt i første kolonne og alle arketypene som inngår i opt'en i andre kolonne. Se forhåndvisningen for eksempel.
////

.Arketyper og OPT installert med DUP
[options="header",frame="topbot",grid="none",cols="40,70"]
|====
|OPT |Arketyper i OPT 

.4+|PROTEHCT_Administration.opt
|openEHR-EHR-COMPOSITION.clinical_trial_protehct.v1
|openEHR-EHR-ADMIN_ENTRY.study_participation.v0
|openEHR-EHR-EVALUATION.protehct_reference_standard.v1
|openEHR-EHR-CLUSTER.paris_classification.v0

.7+|PROTEHCT_Examination.opt
|openEHR-EHR-COMPOSITION.report-procedure.v1
|openEHR-EHR-OBSERVATION.imaging_exam_result_dips.v1
|openEHR-EHR-CLUSTER.image_quality_protehct.v1
|openEHR-EHR-CLUSTER.likert_count_classification_urinary_tract_cancer.v1
|openEHR-EHR-CLUSTER.stones_in_urinary_tract.v1
|openEHR-EHR-CLUSTER.bosniak_classification_protehct.v1
|openEHR-EHR-CLUSTER.person_name.v0


.2+|PROTEHCT_Participation.opt
|openEHR-EHR-COMPOSITION.report-procedure.v1
|openEHR-EHR-CLUSTER.clinical_trial_details.v1

.3+|Protehct_Reference_Standard.opt
|openEHR-EHR-COMPOSITION.report-procedure.v1
|openEHR-EHR-EVALUATION.protehct_reference_standard.v1
|openEHR-EHR-CLUSTER.paris_classification.v0



|====

For ytterligere info knyttet til arketyper og OPT, se <<#archetypewhatis, Arena Arketypeadmin>>.