
PROTEHCT leveres kompatibel med Arena 16.x. I denne versjonen ble AQL konfigurasjon benyttet for å definere gjenbrukbare AQL spørringer til pasientliste. Dokumentasjon av kolonner er gjort i <<PROTEHCT_PASIENTLISTE>>

Distribusjonen inneholder to XML filer med henholdsvis aqlbinding og datelement. Innholdet i disse filene skal legges inn i AQL konfigurasjon i Arena Arkeypeadmin.

* `aql_bindings.xml` inneholder aql bindinger 
* `aql_dataelement.xml` inneholder dataelement 

NOTE: DIPS v/BigMed ressurs kan og vil veilede OUS med å få dette inn i miljøene for test og produkjon. 

