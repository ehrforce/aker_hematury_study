archetype (adl_version=1.4)
	openEHR-EHR-CLUSTER.image_quality_protehct.v1

concept
	[at0000]	-- Bildekvalitet
language
	original_language = <[ISO_639-1::nb]>
description
	original_author = <
		["name"] = <"Bjørn Næss">
		["email"] = <"bna@dips.no">
		["organisation"] = <"DIPS AS">
		["date"] = <"2019-06-24">
	>
	details = <
		["nb"] = <
			language = <[ISO_639-1::nb]>
			purpose = <"Benyttes for å klassifisere kvaliteten på bildet ">
			use = <"Benyttes i sammenheng med radiologiske, og tilsvarende, undersøkelser for å angi kvaliteten på bildet som ble benyttet. ">
			misuse = <"Er kun tiltenkt brukt i forbindelse med radiologiske undersøkelser og angir kvaliteten på bildet. ">
			copyright = <"">
		>
	>
	lifecycle_state = <"0">
	other_contributors = <>
	other_details = <
		["MD5-CAM-1.0.1"] = <"03FAB15B1981F96C681F45B62718DDE2">
	>

definition
	CLUSTER[at0000] matches {	-- Bildekvalitet
		items cardinality matches {1..*; unordered} matches {
			ELEMENT[at0001] occurrences matches {0..1} matches {	-- Kvalitet
				value matches {
					DV_CODED_TEXT matches {
						defining_code matches {
							[local::
							at0002, 	-- Diagnostisk
							at0003]	-- Ikke diagnostisk
						}
					}
				}
			}
			ELEMENT[at0004] occurrences matches {0..1} matches {	-- Kommentar
				value matches {
					DV_TEXT matches {*}
				}
			}
		}
	}

ontology
	term_definitions = <
		["nb"] = <
			items = <
				["at0000"] = <
					text = <"Bildekvalitet">
					description = <"Benyttes for å klassifisere kvaliteten på bildet ">
				>
				["at0001"] = <
					text = <"Kvalitet">
					description = <"*">
				>
				["at0002"] = <
					text = <"Diagnostisk">
					description = <"Kvaliteten på bildet er tilstrekkelig for å gjøre diagnostisk vurdering.">
				>
				["at0003"] = <
					text = <"Ikke diagnostisk">
					description = <"Kvaliteten på bildet er ikke god nok for å gjøre diagnostisk klassifisering.">
				>
				["at0004"] = <
					text = <"Kommentar">
					description = <"Frivillig fritekst kommentar til bildekvaliteten. Vil sannsynligvis benyttes for å forklare hvorfor det ikke er diagnostisk kvalitet på bildet. ">
				>
			>
		>
	>
