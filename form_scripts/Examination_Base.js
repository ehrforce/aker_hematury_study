/**
 * This is the base script which sets some default values 
 */

if (SET_DATAVALUES_ON_INIT) {
    console.log("Setting defaults from script Examination_base.js");
    setBosniak(1);
    setImageQuality(false);


} else {
    console.log("Will not set any defaults from script");
}


function setImageQuality(isDiagnostic) {
    var BILDEKVALITET_FIELD = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/bildekvalitet/kvalitet";
    var val;
    if (isDiagnostic) {
        val = DvCodedText.Parse("local::at0002|Diagnostisk|");
    } else {
        val = DvCodedText.Parse("local::at0003|Ikke diagnostisk|");

    }
    api.setFieldValue(BILDEKVALITET_FIELD,val);
}
function setBosniak(bosniakNumber) {
    var BOSNIAK_KLASSIFISERING = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/bosniak_klassifisering/bosniak_klassifisering";
    if (bosniakNumber < 0) {
        return;
    } else {
        console.log("Set Bosniak");
        var codedText = "local::at0012|Ingen cyster|";
        switch (bosniakNumber) {
            case 1:
                codedText = "local::at0002|Bosniak I|";
                break;
            case 2:
                codedText = "local::at0003|Bosniak II|";
                break;
            case 3:
                codedText = "local::at0004|Bosniak IIF|";
                break;
            case 4:
                codedText = "local::at0005|Bosniak III|";
                break;
            case 5:
                codedText = "local::at0006|Bosniak IV|";
                break;

            default:
                console.log("Default");
        }
        var b = DvCodedText.Parse(codedText);
        api.setFieldValue(BOSNIAK_KLASSIFISERING,b);
    }

}

/**
 * Set LIKERT scale for Blære, Ureter, Nyrebekken og Nyreparenchym. 
 * MAX_MISSING angir hvor mange felter som IKKE skal få verdi. 
 * @param {number} MAX_MISSING 
 * @param {string[]} fields
 */
function setLikertScale(MAX_MISSING,fields) {
    var MISSING = 0;
    fields.forEach(function (field) {
        var skip = getRandomInt(0,2) > 1;
        if (skip && MISSING < MAX_MISSING) {
            MISSING = MISSING + 1;
        } else {
            n = new DvCount();
            n.Magnitude = getRandomInt(1,5);
            api.setFieldValue(field,n);
        }
    });
}

/**
 * Returns an array with only NP fields 
 */
function getLikertArrayExperimental() {
    var NP = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/blære@Blære/np";
    var NP2 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyrebekken@Nyrebekken/np";
    var NP3 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyreparenchym@Nyreparenchym/np";
    var NP4 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/ureter@Ureter/np";
    return [NP,NP2,NP3,NP4];
}

/**
 * Returns an array with all fields with likert scale. 
 * Extracted as separete method for readability. 
 */
function getLikertArray() {
    var CMP = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/blære@Blære/cmp";
    var CMP2 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyrebekken@Nyrebekken/cmp";
    var CMP3 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyreparenchym@Nyreparenchym/cmp";
    var CMP4 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/ureter@Ureter/cmp";

    var EP = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/blære@Blære/ep";
    var EP2 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyrebekken@Nyrebekken/ep";
    var EP3 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyreparenchym@Nyreparenchym/ep";
    var EP4 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/ureter@Ureter/ep";

    var NP = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/blære@Blære/np";
    var NP2 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyrebekken@Nyrebekken/np";
    var NP3 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyreparenchym@Nyreparenchym/np";
    var NP4 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/ureter@Ureter/np";

    var TOMSERIE = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/blære@Blære/tomserie";
    var TOMSERIE2 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyrebekken@Nyrebekken/tomserie";
    var TOMSERIE3 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/nyreparenchym@Nyreparenchym/tomserie";
    var TOMSERIE4 = "protehct_undersøkelse/ct_granskning@CT Granskning/uspesifisert_hendelse/ureter@Ureter/tomserie";
    return [CMP,EP,NP,TOMSERIE,CMP2,EP2,NP2,TOMSERIE2,CMP3,EP3,NP3,TOMSERIE3,CMP4,EP4,NP4,TOMSERIE4];
}
/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min,max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}



