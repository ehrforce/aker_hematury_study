
api.addListener(METODE,"OnFormInitialized",function (id,value,parent) {
    assertMethodHasCorrectCode(value);
})
api.addListener(METODE,"OnChanged",function (id,value,parent) {
    assertMethodHasCorrectCode(value);
})

var CT_DATO = "protehct_undersøkelse/ct_granskning@CT Granskning/bildediagnostisk_rekvisisjon/bildedetaljer/ct_dato@CT Dato";
api.addListener(CT_DATO,"OnFormInitialized",function (id,value,parent) {
    assertCtDateIsNullOnInitializedNewForm(value);
})

// ASSERT FUNCTIONS

function assertMethodHasCorrectCode(value) {
    var testName = "Method has correct value";
    var code = value.DefiningCode.CodeString;
    if (METODE_EXPECTED === code) {
        logTestOk(testName,null);
    } else {
        logTestFailed(testName,null,METODE_EXPECTED,code);

    }

}
function assertCtDateIsNullOnInitializedNewForm(value) {
    var isNew = ctx.isNew();

    var testName = "TEST CT Date should be null on initialized";
    if (isNew) {
        if (value.value === null) {
            logTestOk(testName,null);
        } else {
            logTestFailed(testName,null,null,value.value);
        }
    } else {
        logTestOk(testName,"Assumes ok");
    }

}

// SOME FUNCTIONS TO LOG THE OUTCOME OF TESTS TO MAKE THEM COMPARABLE


/**
 * 
 * @param {string} testName 
 * @param {string} message 
 */
function logTestOk(testName,message) {
    if (LOG_GREEN_TESTS) {
        if (!message) {
            console.log("TEST OK name:" + testName);
        } else {
            console.log("TEST OK name:" + testName + ", message: " + message);
        }

    }
}
function logTestFailed(testName,message,expected,actual) {
    if (!message) {
        console.error("TEST FAILED name:" + testName + ", expected: " + expected + ", actual: " + actual);
    } else {

        console.error("TEST FAILED name:" + testName + ", expected: " + expected + ", actual: " + actual + ", message: " + message);
    }

}